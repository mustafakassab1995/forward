@extends('admin.layout')


@section('content')

    <div id="main">
        <div class="row">
          <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
          <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
            <!-- Search for small screen-->
            <div class="container">
              <div class="row">
                <div class="col s10 m6 l6">
                  <h5 class="breadcrumbs-title mt-0 mb-0">DataTable</h5>
                  <ol class="breadcrumbs mb-0">
                    <li class="breadcrumb-item"><a href="index-2.html">Home</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Table</a>
                    </li>
                    <li class="breadcrumb-item active">DataTable
                    </li>
                  </ol>
                </div>
                {{-- <div class="col s2 m6 l6"><a class="btn dropdown-settings waves-effect waves-light breadcrumbs-btn right" href="#!" data-target="dropdown1"><i class="material-icons hide-on-med-and-up">settings</i><span class="hide-on-small-onl">Settings</span><i class="material-icons right">arrow_drop_down</i></a><ul class="dropdown-content" id="dropdown1" tabindex="0" style="">
                    <li tabindex="0"><a class="grey-text text-darken-2" href="user-profile-page.html">Profile<span class="new badge red">2</span></a></li>
                    <li tabindex="0"><a class="grey-text text-darken-2" href="app-contacts.html">Contacts</a></li>
                    <li tabindex="0"><a class="grey-text text-darken-2" href="page-faq.html">FAQ</a></li>
                    <li class="divider" tabindex="-1"></li>
                    <li tabindex="0"><a class="grey-text text-darken-2" href="user-login.html">Logout</a></li>
                  </ul>

                </div> --}}
              </div>
            </div>
          </div>
          <div class="col s12">
            <div class="container">
              <div class="section section-data-tables">
    <div class="card">
      <div class="card-content">
        <p class="caption mb-0">Tables are a nice way to organize a lot of data. We provide a few utility classes to help
          you style your table as easily as possible. In addition, to improve mobile experience, all tables on
          mobile-screen widths are centered automatically.</p>
      </div>
    </div>
    <!-- Page Length Options -->
   <div class="row">
     <div class="col s12">
       <div class="card">
         <div class="card-content">
           <h4 class="card-title">Page Length Options</h4>
           <div class="row">
             <div class="col s12">
          <form class="form-horizontal"  >
               <div class="col-md-12">


               @foreach ($form_atts as $att)

            @if($att['input']=="input")
            <div class="form-group{{ $errors->has($att['name']) ? ' has-error' : '' }}">
                <label for="{{$att['name']}}" class="col-md-4 control-label">{{$att['label']}}</label>

                <div class="col-md-10">
                    <input id="{{$att['name']}}" type="{{$att['type']}}" class="form-control" name="{{$att['name']}}"
                    @if($att['type']!="password")
                    value="{{ $item[$att['name']] }}"
                  @endif
                    {{($att['required']&&$att['type']!="file") ? 'required' : ''}} disabled>

                    @if ($errors->has($att['name']))
                        <span class="help-block">
                            <strong>{{ $errors->first($att['name']) }}</strong>
                        </span>
                    @endif
                </div>
                @if($att['type']=="file")
                  <div class="col-md-12">
                  <img src="{{ $item[$att['name']]  }}" width="80px" >


                </div>
                @endif
            </div>
            @elseif($att['input']=="textarea")
            <div class="form-group{{ $errors->has($att['name']) ? ' has-error' : '' }}">
                <label for="{{$att['name']}}" class="col-md-4 control-label">{{$att['label']}}</label>

                <div class="col-md-10">
                    <textarea id="{{$att['name']}}"  class="form-control " name="{{$att['name']}}" disabled>{{ $item[$att['name']] }}</textarea>




                </div>

            </div>
            @elseif($att['input']=="icon")

             <div class="form-group{{ $errors->has($att['name']) ? ' has-error' : '' }}">
                <label for="{{$att['name']}}" class="col-md-4 control-label ">{{$att['label']}}</label>

                <div class="col-md-10">
             <i class="fa fa-{{ $item[$att['name']] }}"></i>


                </div>
            </div>

          @elseif($att['input']=="select")
            <div class="form-group{{ $errors->has($att['name']) ? ' has-error' : '' }}">
                <label for="{{$att['name']}}" class="col-md-4 control-label">{{$att['label']}}</label>

                <div class="col-md-10">
                  <select class="form-control" name="{{$att['name']}}" disabled>
                    @foreach ($att['data'] as $s_data)

                      <option value="{{$s_data['key']}}"
                      {{($s_data['key']==$item[$att['name']]) ? 'selected' : ''}}>
                        {{$s_data['label']}} </option>
                    @endforeach

                  </select>

                  @if ($errors->has($att['name']))
                      <span class="help-block">
                          <strong>{{ $errors->first($att['name']) }}</strong>
                      </span>
                  @endif
                </div>
            </div>

          @endif

          @endforeach

            <div class="form-group">

          </div>



          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>



@endsection
