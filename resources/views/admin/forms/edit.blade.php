@extends('admin.nlayout')
@section('style')
	<!-- Custom CSS -->
	<link href="{{asset('admin/dist/css/style.css')}}" rel="stylesheet" type="text/css">
	<style type="text/css">
		.form-group{
    		overflow: hidden;
		}
	</style>
@endsection
@section('content')

	      <div class="wrapper">
	          <div class="container-fluid">

	              <!-- start page title -->
	              <div class="row">
	                  <div class="col-12">
	                      <div class="page-title-box">
	                          <div class="page-title-right">
	                              <ol class="breadcrumb m-0">
	                                  <li class="breadcrumb-item"><a href="javascript: void(0);">اللوحة</a></li>
	                                  <li class="breadcrumb-item"><a href="javascript: void(0);">{{$title_ar}}</a></li>
	                                  <li class="breadcrumb-item active">انشاء {{$title_ar}}</li>
	                              </ol>
	                          </div>
	                          <h4 class="page-title">{{$title_ar}}</h4>
	                      </div>
	                  </div>
	              </div>
	<div class="row">
                    <div class="col-12">
                        <div class="card-box">

                           <li class="breadcrumb-item active">انشاء {{$title_ar}}</li>

                            <div class="row">
                                <div class="col-xl-12">

              <form class="col s12" method="{{$form_method}}" action="{{route($form_action,$item['id'])}}"
              enctype="{{($form_multipart) ? 'multipart/form-data' : ''}}" >
                   {{csrf_field()}}
                   <div class="col-md-12">
                     <input type="hidden" name="_method" value="put">

                   @foreach ($form_atts as $att)

                @if($att['input']=="input")
                <div class="form-group{{ $errors->has($att['name']) ? ' has-error' : '' }}">
                    <label for="{{$att['name']}}" class="col-md-4 control-label">{{$att['label']}}</label>

                    <div class="col-md-10">
                        <input id="{{$att['name']}}" type="{{$att['type']}}" class="form-control" name="{{$att['name']}}"
                        @if($att['type']!="password")
                        value="{{ $item[$att['name']] }}"
                      @endif
                        {{($att['required']&&$att['type']!="file") ? 'required' : ''}} >

                        @if ($errors->has($att['name']))
                            <span class="help-block">
                                <strong>{{ $errors->first($att['name']) }}</strong>
                            </span>
                        @endif
                    </div>
                    @if($att['type']=="file")
                        <div class="col-md-12">

                            <img src="{{ asset($item[$att['name']])  }}" width="80px" >





                    </div>
                    @endif
                </div>
              @elseif($att['input']=="multi")

                        <div class="form-group{{ $errors->has($att['name']) ? ' has-error' : '' }}">
                            <label for="{{$att['name']}}" class="col-md-4 control-label">{{$att['label']}}</label>

                            <div class="col-md-10">
                              <select class="form-control js-example-basic-multiple" name="{{$att['name']}}" multiple="multiple">
                                @foreach ($att['data'] as $s_data)
                                  @php
                                    $kj=(array) $item[$att['name']];
                                  @endphp
                                  <option value="{{$s_data['key']}}"   {{(\in_array($s_data['key'],$kj)) ? 'selected' : ''}}>{{$s_data['label']}} </option>
                                @endforeach

                              </select>

                              @if ($errors->has($att['name']))
                                  <span class="help-block">
                                      <strong>{{ $errors->first($att['name']) }}</strong>
                                  </span>
                              @endif
                            </div>
                        </div>
                @elseif($att['input']=="textarea")
                <div class="form-group{{ $errors->has($att['name']) ? ' has-error' : '' }}">
                    <label for="{{$att['name']}}" class="col-md-4 control-label">{{$att['label']}}</label>

                    <div class="col-md-10">
                        <textarea id="{{$att['name']}}" rows="10"   class="mytextarea" name="{{$att['name']}}">{{ $item[$att['name']] }}</textarea>



                        @if ($errors->has($att['name']))
                            <span class="help-block">
                                <strong>{{ $errors->first($att['name']) }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
                @elseif($att['input']=="icon")

                 <div class="form-group{{ $errors->has($att['name']) ? ' has-error' : '' }}">
                    <label for="{{$att['name']}}" class="col-md-4 control-label ">{{$att['label']}}</label>

                    <div class="col-md-10">
                 <i class="fa fa-{{ $item[$att['name']] }}"></i>
                     <select class="form-control wpmse_select2" name="{{$att['name']}}" >

                         </select>
              @if ($errors->has($att['name']))
                          <span class="help-block">
                              <strong>{{ $errors->first($att['name']) }}</strong>
                          </span>
                      @endif
                    </div>
                </div>

              @elseif($att['input']=="select")
                <div class="form-group{{ $errors->has($att['name']) ? ' has-error' : '' }}">
                    <label for="{{$att['name']}}" class="col-md-4 control-label">{{$att['label']}}</label>

                    <div class="col-md-10">
                      <select class="form-control" name="{{$att['name']}}" >
                        @foreach ($att['data'] as $s_data)

                          <option value="{{$s_data['key']}}"
                          {{($s_data['key']==$item[$att['name']]) ? 'selected' : ''}}>
                            {{$s_data['label']}} </option>
                        @endforeach

                      </select>

                      @if ($errors->has($att['name']))
                          <span class="help-block">
                              <strong>{{ $errors->first($att['name']) }}</strong>
                          </span>
                      @endif
                    </div>
                </div>

              @endif

						@endforeach

						<button type="submit" class="btn btn-primary">حفظ</button>
																	</form>
															</div><!-- end col -->

														</div><!-- end row -->
									 </div>
							 </div><!-- end col -->
					 </div>
					 <!-- end row -->
@endsection
@section('script')
<!-- JavaScript -->

  <!-- jQuery -->
  <script src="{{asset('admin/vendors/bower_components/jquery/dist/jquery.min.js')}}"></script>

  <!-- Bootstrap Core JavaScript -->
  <script src="{{asset('admin/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('admin/vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js')}}"></script>

  <!-- Slimscroll JavaScript -->
  <script src="{{asset('admin/dist/js/jquery.slimscroll.js')}}"></script>

  <!-- Fancy Dropdown JS -->
  <script src="{{asset('admin/dist/js/dropdown-bootstrap-extended.js')}}"></script>

  <!-- Owl JavaScript -->
  <script src="{{asset('admin/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js')}}"></script>

  <!-- Switchery JavaScript -->
  <script src="{{asset('admin/vendors/bower_components/switchery/dist/switchery.min.js')}}"></script>

  <!-- Init JavaScript -->
  <script src="{{asset('admin/dist/js/init.js')}}"></script>
@endsection
@section('alert')

@if(session()->has('success'))
<script type="text/javascript">
$(function() {
"use strict";

var SweetAlert = function() {};

  //examples
  SweetAlert.prototype.init = function() {

  //Success Message
  setTimeout(function(){
    swal({
    title: "عملية ناجحة",
           type: "success",
    text: "تمت العملية بنجاح",
    confirmButtonColor: "#8BC34A",
    timer: 2000,
          showConfirmButton: false
      });
   }, 3000);


  },
  //init
  $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert;

$.SweetAlert.init();
});
</script>
@endif
@endsection
