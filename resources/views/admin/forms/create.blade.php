@extends('admin.nlayout')
@section('style')
	<!-- Custom CSS -->
	<link href="{{asset('admin/dist/css/style.css')}}" rel="stylesheet" type="text/css">
	<style type="text/css">
		.form-group{
    		overflow: hidden;
		}
	</style>
@endsection
@section('content')

	      <div class="wrapper">
	          <div class="container-fluid">

	              <!-- start page title -->
	              <div class="row">
	                  <div class="col-12">
	                      <div class="page-title-box">
	                          <div class="page-title-right">
	                              <ol class="breadcrumb m-0">
	                                  <li class="breadcrumb-item"><a href="javascript: void(0);">اللوحة</a></li>
	                                  <li class="breadcrumb-item"><a href="javascript: void(0);">{{$title_ar}}</a></li>
	                                  <li class="breadcrumb-item active">انشاء {{$title_ar}}</li>
	                              </ol>
	                          </div>
	                          <h4 class="page-title">{{$title_ar}}</h4>
	                      </div>
	                  </div>
	              </div>
	<div class="row">
                    <div class="col-12">
                        <div class="card-box">

                           <li class="breadcrumb-item active">انشاء {{$title_ar}}</li>

                            <div class="row">
                                <div class="col-xl-12">

              <form class="form-horizontal" method="{{$form_method}}" action="{{route($form_action)}}"
               enctype="{{($form_multipart) ? 'multipart/form-data' : ''}}" >
                   {{csrf_field()}}
                   <div class="col-md-12">

                   @foreach ($form_atts as $att)

                @if($att['input']=="input")
                <div class="form-group{{ $errors->has($att['name']) ? ' has-error' : '' }}">
                    <label for="{{$att['name']}}" class="col-md-4 control-label">{{$att['label']}}</label>

                    <div class="col-md-10">
                      @if($att['type']=="file")
                        <input class="form-control" type="file"  class="file" name="{{$att['name']}}"/>
                        <br>



      @elseif($att['type']=="number")
        <input id="{{$att['name']}}" type="{{$att['type']}}" class="form-control" name="{{$att['name']}}" value="{{ old($att['name']) }}" required >

                    @else
                        <input id="{{$att['name']}}" type="{{$att['type']}}" class="form-control" name="{{$att['name']}}" value="{{ old($att['name']) }}" required >
                      @endif
                        @if ($errors->has($att['name']))
                            <span class="help-block">
                                <strong>{{ $errors->first($att['name']) }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                @elseif($att['input']=="textarea")
                <div class="form-group{{ $errors->has($att['name']) ? ' has-error' : '' }}">
                    <label for="{{$att['name']}}" class="col-md-4 control-label">{{$att['label']}}</label>

                    <div class="col-md-10">
                        <textarea id="{{$att['name']}}" rows="10" class="mytextarea" name="{{$att['name']}}"  >{{ old($att['name']) }}</textarea>

                        @if ($errors->has($att['name']))
                            <span class="help-block">
                                <strong>{{ $errors->first($att['name']) }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

              @elseif($att['input']=="multi")

                        <div class="form-group{{ $errors->has($att['name']) ? ' has-error' : '' }}">
                            <label for="{{$att['name']}}" class="col-md-4 control-label">{{$att['label']}}</label>

                            <div class="col-md-10">
                              <select class="form-control js-example-basic-multiple" name="{{$att['name']}}" multiple="multiple">
                                @foreach ($att['data'] as $s_data)
                                  <option value="{{$s_data['key']}}" >{{$s_data['label']}} </option>
                                @endforeach

                              </select>

                              @if ($errors->has($att['name']))
                                  <span class="help-block">
                                      <strong>{{ $errors->first($att['name']) }}</strong>
                                  </span>
                              @endif
                            </div>
                        </div>
              @elseif($att['input']=="icon")

               <div class="form-group{{ $errors->has($att['name']) ? ' has-error' : '' }}">
                  <label for="{{$att['name']}}" class="col-md-4 control-label ">{{$att['label']}}</label>

                  <div class="col-md-10">
                   <select class="form-control wpmse_select2" name="{{$att['name']}}" >

                       </select>
            @if ($errors->has($att['name']))
                        <span class="help-block">
                            <strong>{{ $errors->first($att['name']) }}</strong>
                        </span>
                    @endif
                  </div>
              </div>

              @elseif($att['input']=="select")
                <div class="form-group{{ $errors->has($att['name']) ? ' has-error' : '' }}">
                    <label for="{{$att['name']}}" class="col-md-4 control-label">{{$att['label']}}</label>

                    <div class="col-md-10">
                      <select class="form-control" name="{{$att['name']}}" >
                        @foreach ($att['data'] as $s_data)
                          <option value="{{$s_data['key']}}">{{$s_data['label']}} </option>
                        @endforeach

                      </select>

                      @if ($errors->has($att['name']))
                          <span class="help-block">
                              <strong>{{ $errors->first($att['name']) }}</strong>
                          </span>
                      @endif
                    </div>
                </div>

              @endif

              @endforeach

							<button type="submit" class="btn btn-primary">حفظ</button>
                                    </form>
                                </div><!-- end col -->

															</div><!-- end row -->
										 </div>
								 </div><!-- end col -->
						 </div>
						 <!-- end row -->
@endsection

@section('alert')

  @if(Session::has('message'))

  @if(Session::get('status', 'info')=="success")
<script type="text/javascript">
	$(function() {
	"use strict";

	var SweetAlert = function() {};

    //examples
    SweetAlert.prototype.init = function() {

    //Success Message
    setTimeout(function(){
    	swal({
			title: "عملية ناجحة",
             type: "success",
			text: "تمت العملية بنجاح",
			confirmButtonColor: "#8BC34A",
			timer: 2000,
            showConfirmButton: false
        });
     }, 3000);


    },
    //init
    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert;

	$.SweetAlert.init();
});
</script>
@endif
@endif
@endsection
