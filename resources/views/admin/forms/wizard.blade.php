@extends('admin.layout')

@section('content')

  <div id="main">
  <div class="row">
    <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
    <div class="breadcrumbs-dark pb-0 pt-4" id="breadcrumbs-wrapper">
      <!-- Search for small screen-->
      <div class="container">
        <div class="row">
          <div class="col s10 m6 l6">
            <h5 class="breadcrumbs-title mt-0 mb-0">Create {{$title}}</h5>
            <ol class="breadcrumbs mb-0">

              <li class="breadcrumb-item"><a href="#">{{$title}}</a>
              </li>
              <li class="breadcrumb-item active">Create
              </li>
            </ol>
          </div>
          <div class="col s2 m6 l6"><a class="btn dropdown-settings waves-effect waves-light breadcrumbs-btn right" href="#!" data-target="dropdown1"><i class="material-icons hide-on-med-and-up">settings</i><span class="hide-on-small-onl">Settings</span><i class="material-icons right">arrow_drop_down</i></a><ul class="dropdown-content" id="dropdown1" tabindex="0" style="">
              <li tabindex="0"><a class="grey-text text-darken-2" href="{{route($title.'.create')}}">Create New {{$title}}<span class="new badge red">2</span></a></li>
            </ul>

          </div>
        </div>
      </div>
    </div>
    <div class="col s12">
      <div class="container">
        <div class="seaction">


<!--Basic Form-->

<!-- jQuery Plugin Initialization -->
<div class="row">
  <div class="col s12 m12 l12">
        <div id="Form-advance" class="card card card-default scrollspy">
          <div class="card-content">
            <h4 class="card-title">Create {{$title}}</h4>
          <form class="form-horizontal" method="{{$form_method}}" action="{{route($form_action)}}"
           enctype="{{($form_multipart) ? 'multipart/form-data' : ''}}" >
               {{csrf_field()}}
               <div class="col-md-12">
@php
$i = 1;
$j=1;
 @endphp
   <ul class="stepper horizontal" id="horizStepper">
               @foreach ($form_atts as $att)
@if($i==1||$i%4==0)
  <li class="step {{ ($i==1)? 'active' : '' }}">
                          <div class="step-title waves-effect">Step {{$j}}</div>
                          <div class="step-content">
                             <div class="row">
                               @php
                                 $j++;
                               @endphp
                             @endif
            @if($att['input']=="input")
            <div class="form-group{{ $errors->has($att['name']) ? ' has-error' : '' }}">
                <label for="{{$att['name']}}" class="col-md-4 control-label">{{$att['label']}}</label>

                <div class="col-md-10">
                  @if($att['type']=="file")
                    <div class="file-field input-field">
                        <div class="btn">
                          <span>File</span>
                          <input type="file">
                        </div>
                        <div class="file-path-wrapper">
                          <input class="file-path validate" type="text"name="{{$att['name']}}">
                        </div>
                      </div>




                @else
                    <input id="{{$att['name']}}" type="{{$att['type']}}" class="form-control" name="{{$att['name']}}" value="{{ old($att['name']) }}" required >
                  @endif
                    @if ($errors->has($att['name']))
                        <span class="help-block">
                            <strong>{{ $errors->first($att['name']) }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            @elseif($att['input']=="textarea")
            <div class="form-group{{ $errors->has($att['name']) ? ' has-error' : '' }}">
                <label for="{{$att['name']}}" class="col-md-4 control-label">{{$att['label']}}</label>

                <div class="col-md-10">
                    <textarea id="{{$att['name']}}" rows="10" class="form-control {{($att['type']!="textarea") ? 'mytextarea' : ''}}" name="{{$att['name']}}"  >{{ old($att['name']) }}</textarea>

                    @if ($errors->has($att['name']))
                        <span class="help-block">
                            <strong>{{ $errors->first($att['name']) }}</strong>
                        </span>
                    @endif
                </div>
            </div>

          @elseif($att['input']=="multi")
              
                    <div class="form-group{{ $errors->has($att['name']) ? ' has-error' : '' }}">
                        <label for="{{$att['name']}}" class="col-md-4 control-label">{{$att['label']}}</label>

                        <div class="col-md-10">
                          <select class="form-control js-example-basic-multiple" name="{{$att['name']}}" multiple="multiple">
                            @foreach ($att['data'] as $s_data)
                              <option value="{{$s_data['key']}}">{{$s_data['label']}} </option>
                            @endforeach

                          </select>

                          @if ($errors->has($att['name']))
                              <span class="help-block">
                                  <strong>{{ $errors->first($att['name']) }}</strong>
                              </span>
                          @endif
                        </div>
                    </div>
          @elseif($att['input']=="icon")

           <div class="form-group{{ $errors->has($att['name']) ? ' has-error' : '' }}">
              <label for="{{$att['name']}}" class="col-md-4 control-label ">{{$att['label']}}</label>

              <div class="col-md-10">
               <select class="form-control wpmse_select2" name="{{$att['name']}}" >

                   </select>
        @if ($errors->has($att['name']))
                    <span class="help-block">
                        <strong>{{ $errors->first($att['name']) }}</strong>
                    </span>
                @endif
              </div>
          </div>

          @elseif($att['input']=="select")
            <div class="form-group{{ $errors->has($att['name']) ? ' has-error' : '' }}">
                <label for="{{$att['name']}}" class="col-md-4 control-label">{{$att['label']}}</label>

                <div class="col-md-10">
                  <select class="form-control" name="{{$att['name']}}" >
                    @foreach ($att['data'] as $s_data)
                      <option value="{{$s_data['key']}}">{{$s_data['label']}} </option>
                    @endforeach

                  </select>

                  @if ($errors->has($att['name']))
                      <span class="help-block">
                          <strong>{{ $errors->first($att['name']) }}</strong>
                      </span>
                  @endif
                </div>
            </div>

          @endif

@if(count($form_atts)<=$i)
  <div class="step-actions">
                                     <div class="row">
                                         <div class="col m6 s12 mb-1">
                                             <button class="red btn mr-1 btn-reset" type="reset">
                                                 <i class="material-icons">clear</i>
                                                 Reset
                                             </button>
                                         </div>
                                         <div class="col m6 s12 mb-1">
                                             <button class="waves-effect waves-dark btn btn-primary"
                                                 type="submit">Submit</button>
                                         </div>
                                     </div>
                                 </div>
@elseif(($i%3==0))
<div class="step-actions">
                                   <div class="row">
                                       <div class="col m4 s12 mb-3">
                                           <button class="red btn btn-reset" type="reset">
                                               <i class="material-icons left">clear</i>Reset
                                           </button>
                                       </div>
                                       <div class="col m4 s12 mb-3">
                                           <button class="btn btn-light previous-step">
                                               <i class="material-icons left">arrow_back</i>
                                               Prev
                                           </button>
                                       </div>
                                       <div class="col m4 s12 mb-3">
                                           <button class="waves-effect waves dark btn btn-primary next-step"
                                               type="submit">
                                               Next
                                               <i class="material-icons right">arrow_forward</i>
                                           </button>
                                       </div>
                                   </div>
                               </div>
                             @endif

                             @if($i==3||$i==7||$i==10)
                                     </div>
                                     </div>
                                                     </li>
                             @endif
                             @php
                             $i++;
                             @endphp
          @endforeach
</ul>
            {{-- <div class="form-group">
                    <button type="submit" class="btn btn-success ">
                    Create
                    </button>
          </div> --}}



          </div>
          </form>
        </div>
      </div>
      </div>
      </div>
      </div>
      </div>
      </div>
      </div>
      </div>
      {{-- </div> --}}



@endsection
