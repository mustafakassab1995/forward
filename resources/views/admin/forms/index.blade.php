@extends('admin.nlayout')


@section('content')
  <!-- ============================================================== -->
      <!-- Start Page Content here -->
      <!-- ============================================================== -->

      <div class="wrapper">
          <div class="container-fluid">

              <!-- start page title -->
              <div class="row">
                  <div class="col-12">
                      <div class="page-title-box">
                          <div class="page-title-right">
                              <ol class="breadcrumb m-0">
                                  <li class="breadcrumb-item"><a href="javascript: void(0);">اللوحة</a></li>
                                  <li class="breadcrumb-item"><a href="javascript: void(0);">{{$title_ar}}</a></li>
                                  <li class="breadcrumb-item active">عرض ال {{$title_ar}}</li>
                              </ol>
                          </div>
                          <h4 class="page-title">{{$title_ar}}</h4>
                      </div>
                  </div>
              </div>
              <!-- end page title -->

              <div class="row">
                  <div class="col-12">
                      <div class="card-box">
                          <h4 class="header-title">العرض</h4>
                          <p class="sub-header">
                                <a href="{{route($title.'.create')}}" class="btn btn-success">اضافة عنصر جديد </a>
                          </p>

                          <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                              <thead>
                              <tr>
                                <tr>
                               @foreach ($form_atts as $coulmn)

                                     @if($coulmn['input']!="textarea"&&$coulmn['type']!=="password"&&$coulmn['type']!=="multi"&&$coulmn['type']!=="disabled")

                               <th>
                                 {{$coulmn['label']}}

                                 </th>
                                   @endif
                               @endforeach

                               <th>
                               التحكم
                               </th>
                              </tr>
                              </thead>


                              <tbody>
                                @foreach ($instances as $instance)
                         <tr>

                           @for ($j=0; $j < count($form_atts); $j++)

                               @if(($form_atts[$j]['type']==="date"||$form_atts[$j]['type']==="time"||

                               $form_atts[$j]['type']==="email"||$form_atts[$j]['type']==="number"
                               ||$form_atts[$j]['type']==="text")&&$form_atts[$j]['input']!="textarea"&&$form_atts[$j]['input']!="select")
                               <td>{{$instance[$form_atts[$j]['name']]}}</td>
                             @elseif($form_atts[$j]['type']==="file")
                             <td>  <img src="{{ asset($instance[$form_atts[$j]['name']])  }}" width="80px" ></td>

                           @elseif($form_atts[$j]['type']==="icon")
                             <td> <i class="fa fa-{{ $instance[$form_atts[$j]['name']] }}"></i></td>

                           @elseif($form_atts[$j]['type']==="select"||$form_atts[$j]['input']==="select")
                          <td>   {{getValueK($instance[$form_atts[$j]['name']], $form_atts[$j]['data'])}}
                          </td>

                           @elseif($form_atts[$j]['input']==="textarea"||$form_atts[$j]['type']==="password")

                             @endif




                           @endfor
                                  <td>
                                      <a href="{{ route($title.'.edit',$instance['id']) }}" data-toggle="tooltip" data-original-title="عرض"> <i class="fa fa-eye" style="padding-right:10px"></i> </a>
                                      <form name="dform{{$instance['id']}}" action="{{ route($title.'.destroy',$instance['id']) }}" method="post">
                                        <input type="hidden" name="_method" value="delete">
                                        @csrf
                                      </form>
                                      <a href="javascript:document.dform{{$instance['id']}}.submit();" id="delete-btn"  data-toggle="tooltip" data-original-title="حذف"> <i class="fa fa-trash -o text-danger" style="padding-right:10px"></i></a>
                                      @if(\array_key_exists("type",$instance))
                                        @if($instance['type']=="client")
                                        <a href="{{route('Order.index')."?client_id=".$instance['id']}}" data-toggle="tooltip" data-original-title="عرض"> عرض طلبات المستخدم </a>
                                      @endif
                                      @endif
                                  </td>
                              </tr>
                                  @endforeach

                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
              <!-- end row -->



          </div> <!-- end container-fluid -->
      </div>
      <!-- end wrapper -->

      <!-- ============================================================== -->
      <!-- End Page content -->
      <!-- ============================================================== -->

@endsection
