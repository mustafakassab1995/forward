@extends('admin.nlayout')
@section('content')
<div class="row">
    <div class="col-md-6 col-xl-3">
        <div class="card-box tilebox-one">
            <i class="icon-layers float-right m-0 h2 text-muted"></i>
            <h6 class="text-muted text-uppercase mt-0">الطلبات</h6>
            <h3 class="my-3" data-plugin="counterup">{{\App\Order::orderBy('id','desc')->count()}}</h3>
            <span class="badge badge-success mr-1"> +11% </span> <span class="text-muted">الزيادة الحالية</span>
        </div>
    </div>

    <div class="col-md-6 col-xl-3">
        <div class="card-box tilebox-one">
            <i class="icon-paypal float-right m-0 h2 text-muted"></i>
            <h6 class="text-muted text-uppercase mt-0">الأرباح</h6>
            <h3 class="my-3">$<span data-plugin="counterup">{{\DB::table('orders')->where('status',5)->sum('total')}}</span></h3>
            <span class="badge badge-danger mr-1"> +29% </span> <span class="text-muted">الزيادة الحالية</span>
        </div>
    </div>

    <div class="col-md-6 col-xl-3">
        <div class="card-box tilebox-one">
            <i class="icon-chart float-right m-0 h2 text-muted"></i>
            <h6 class="text-muted text-uppercase mt-0">السعر المتوسط للطلبات</h6>
            <h3 class="my-3">$<span data-plugin="counterup">{{\DB::table('orders')->avg('total')}}</span></h3>
              <span class="badge badge-danger mr-1"> +29% </span> <span class="text-muted">الزيادة الحالية</span>
        </div>
    </div>

    <div class="col-md-6 col-xl-3">
        <div class="card-box tilebox-one">
            <i class="icon-rocket float-right m-0 h2 text-muted"></i>
            <h6 class="text-muted text-uppercase mt-0">عدد الطلبات الناجحة</h6>
            <h3 class="my-3" data-plugin="counterup">{{\DB::table('orders')->where('status',5)->count()}}</h3>
            <span class="badge badge-warning mr-1"> +89% </span> <span class="text-muted">الزيادة الحالية</span>
        </div>
    </div>
</div>
<!-- end row -->

<div class="row">
    <div class="col-xl-7">
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                    <h4 class="header-title mb-3">آخر المستخدمين</h4>

                    <div class="inbox-widget slimscroll" style="max-height: 324px;">
                      @php
                      $ds =   \App\User::orderBy('id','desc')->take(20)->get();
                      @endphp
                        @foreach ($ds as $d)
                        <a href="{{route('User.edit',$d->id)}}">
                            <div class="inbox-item">
                                <div class="inbox-item-img"><img src="{{asset('nass/images/users/avatar-1.jpg')}}" class="rounded-circle" alt=""></div>
                                <p class="inbox-item-author">{{$d->name}}</p>
                                <p class="inbox-item-text">{{$d->phone}}</p>
                                <p class="inbox-item-date">{{$d->created_at}}</p>
                            </div>
                        </a>
                      @endforeach


                    </div>

                </div>
            </div>



        </div>
    </div><!-- end col-->

    {{-- <div class="col-xl-5">
        <div class="card-box">

            <h4 class="header-title mb-3">آخر الدفعات المالية</h4>

            <div class="table-responsive">
                <table class="table table-bordered table-nowrap mb-0">
                    <thead>
                        <tr>
                            <th>دفعة من</th>
                            <th>الطلب</th>
                            <th>المبلغ</th>
                            <th>الحالة</th>
                        </tr>
                    </thead>
                    <tbody>
                      @php
                        $orders = \App\Order::where('status',4)->orderBy('id','desc')->with('borrower')->take(6)->get();
                      @endphp
                      @foreach ($orders as $order)
                        <tr>
                            <th class="text-muted">{{$order->borrower->name}}</th>
                            <td>{{$order->id}}</td>
                            <td>{{$order->total}}</td>
                            <td><span class="badge badge-success">تم الدفع</span></td>
                        </tr>
                      @endforeach


                    </tbody>
                </table>
            </div>
        </div>
    </div><!-- end col-->

    <div class="col-xl-12">
        <div class="card-box">

            <h4 class="header-title mb-3">سيارات بانتظار رد الادارة</h4>

            <div class="table-responsive">
                <table class="table table-bordered table-nowrap mb-0">
                    <thead>
                        <tr>
                           <th>اسم السيارة</th>
                            <th>صاحب السيارة</th>
                             <th>الصورة</th>
                              <th>مزيد من التفاصيل</th>
                              <th>تغيير الحالة</th>
                        </tr>
                    </thead>
                    <tbody>
                      @php
                        $cars = \App\Car::where('status',1)->orderBy('id','desc')->with('user')->take(10)->get();
                      @endphp
                      @foreach ($cars as $car)
                        <tr>
                            <th class="text-muted">{{$car->title}}</th>
                            <td>{{$car->user->name}}</td>
                            <td> <img src="{{asset($car->full_photo)}}" width="100px"></td>
                            <td>  <a href="{{ route('Car.edit',$car->id) }}" data-toggle="tooltip" data-original-title="عرض"> <i class="fa fa-eye" style="padding-right:10px"></i> </a></td>

                            <td>
                                <a href="{{ url('admin/acceptCar?id='.$car->id) }}" class="btn btn-success">موافقة </a>
                                 <a href="{{ url('admin/rejectCar?id='.$car->id) }}" class="btn btn-danger">رفض </a>
                                </td>
                        </tr>
                      @endforeach


                    </tbody>
                </table>
            </div>
        </div>
    </div><!-- end col--> --}}

</div>
@endsection
