<div class="topbar-menu">
    <div class="container-fluid">
        <div id="navigation">
            <!-- Navigation Menu-->
            <ul class="navigation-menu">

                <li class="has-submenu">
                    <a href="{{url('admin/index')}}">
                        <i class="mdi mdi-view-dashboard"></i>الرئيسية
                    </a>
                </li>

                <li class="has-submenu">
                    <a href="#">
                        <i class="mdi mdi-format-underline"></i>ادارة الطلبات <div class="arrow-down"></div>
                    </a>
                    <ul class="submenu megamenu">
                        <li>
                            <ul>
                              <li><a href="{{route('Order.index')}}">كل الطلبات</a></li>
                                {{-- <li><a href="{{route('Order.index')."?status=1"}}">الطلبات الحالية</a></li>--}}
                            </ul>
                        </li>
                    </ul>
                </li>



                <li class="has-submenu">
                    <a href="#">
                        <i class="mdi mdi-package-variant-closed"></i>ادارة السائقين <div class="arrow-down"></div></a>
                        <ul class="submenu">
                            <li><a href="{{route('Driver.index')}}">كل السائقين</a></li>

                        </ul>
                </li>
                <li class="has-submenu">
                    <a href="#">
                        <i class="mdi mdi-package-variant-closed"></i>ادارة المستخدمين <div class="arrow-down"></div></a>
                        <ul class="submenu">
                            <li><a href="{{route('User.index')}}">المستخدمين</a></li>
                        </ul>
                </li>

                <li class="has-submenu">
                    <a href="#">
                        <i class="mdi mdi-package-variant-closed"></i>ادارة اصحاب الاعمال <div class="arrow-down"></div></a>
                        <ul class="submenu">
                            <li><a href="{{route('Business.index')}}">كل اصحاب الأعمال</a></li>
                        </ul>
                </li>
                <li class="has-submenu">
                    <a href="#">
                        <i class="mdi mdi-package-variant-closed"></i>ادارة الزبائن  <div class="arrow-down"></div></a>
                        <ul class="submenu">
                            <li><a href="{{route('Client.index')}}">كل الزبائن </a></li>
                        </ul>
                </li>

                <li class="has-submenu">
                    <a href="#">
                        <i class="mdi mdi-package-variant-closed"></i>ادارة المحصلين  <div class="arrow-down"></div></a>
                        <ul class="submenu">
                            <li><a href="{{route('Collector.index')}}">كل المحصلين </a></li>
                        </ul>
                </li>

                {{-- <li class="has-submenu">
                    <a href="#"> <i class="mdi mdi-flip-horizontal"></i>الثوابت <div class="arrow-down"></div></a>
                    <ul class="submenu">
                      @php

                        $ddk[]=['name'=>"Setting",'label'=>"ثوابت التطبيق"];
                        $ddk[]=['name'=>"Advantage",'label'=>"ميزات السيارات"];
                      $ddk  []=['name'=>"Carmodel",'label'=>"موديلات السيارات"];
                      @endphp

                      @foreach ($ddk  as $inc)
                        <li >
                            <a href="{{route($inc['name'].'.index')}}">{{$inc['label']}} </a>
                        </li>
                      @endforeach

                    </ul>
                </li> --}}




            </ul>
            <!-- End navigation menu -->

            <div class="clearfix"></div>
        </div>
        <!-- end #navigation -->
    </div>
    <!-- end container -->
</div>
<!-- end navbar-custom -->
