<!DOCTYPE html>
<html lang="ar">
<head>
    <base href="{{ URL::asset('/') }}"/>
    <title>
      Syartech
        |
        @yield('title')
    </title>
    <!-- META SECTION -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- END META SECTION -->
    <!-- CSS INCLUDE -->
    <link rel="stylesheet" href="{{asset('admin/css/styles.css')}}">
    <link rel="stylesheet" href="{{asset('admin/css/bootstrap-rtl.min.css')}}">

    <!-- EOF CSS INCLUDE -->
    @yield('header')

</head>
<body>


<!-- APP WRAPPER -->
<div class="app app-rtl">

    <!-- START APP CONTAINER -->
    <div class="app-container">

        <!-- START SIDEBAR -->
        <div class="app-sidebar app-navigation app-navigation-style-default dir-right" data-type="close-other">
            <a href="/admin"></a>
            <h3 style="color:white; text-align: center;line-height: 80px">Education system</h3>
            @include('admin.nav')
        </div>
        <!-- END SIDEBAR -->

        <!-- START APP CONTENT -->
        <div class="app-content app-sidebar-right">
            <!-- START APP HEADER -->
            <div class="app-header">
                <ul class="app-header-buttons">
                    <li>
                        <div class="contact contact-rounded contact-bordered contact-lg contact-ps-controls">
                            <img src="{{asset('assets/images/prlogoAsset 1.png')}}" alt="">
                            <div class="contact-container">
                                <a href="#">kk</a>
                                <span>مدير للموقع</span>
                            </div>
                            <div class="contact-controls">
                                <div class="dropdown">
                                    <button type="button" class="btn btn-default btn-icon" data-toggle="dropdown"><span class="icon-cog"></span></button>
                                    <ul class="dropdown-menu dropdown-left">
                                        <li>  <a href="{{ route('logout') }}"
                                                 onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                <span class="icon-exit-right"></span> تسجيل الخروج
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>

                <ul class="app-header-buttons pull-right">
                    <li class="visible-mobile"><a href="#" class="btn btn-link btn-icon" data-sidebar-toggle=".app-sidebar.dir-right"><span class="icon-menu"></span></a></li>
                    <li class="hidden-mobile"><a href="#" class="btn btn-link btn-icon" data-sidebar-minimize=".app-sidebar.dir-right"><span class="icon-list"></span></a></li>
                </ul>
            </div>
            <!-- END APP HEADER  -->

            @yield('content')

        </div>
        <!-- END APP CONTENT -->

    </div>
    <!-- END APP CONTAINER -->

    <!-- START APP FOOTER
    <div class="app-footer app-footer-default" id="footer">

        <div class="alert alert-primary alert-dismissible alert-inside text-center">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="icon-cross"></span></button>

            نقوم باستخدام الكوكيز من اجل سهوله تحميل الموقع والتنقل في لوحة التحكم
        </div>

        <div class="app-footer-line extended">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <h3 class="title"> موقع </h3>
                    <p></p
                </div>
                <div class="col-md-4 col-sm-4">
                    <h3 class="title"><span class="icon-clipboard-text"></span> بعض الروابط المهمة </h3>
                    <ul class="list-unstyled">
                        <li><a href="/dashboard">الصفحة الرئيسية</a></li>
                    </ul>
                </div>
                <div class="col-md-4 col-sm-6">
                    <!--<h3 class="title"><span class="icon-thumbs-up"></span> روابط السوشيال ميديا</h3>-->

                    <!--<a href="#" class="label-icon label-icon-footer label-icon-bordered label-icon-rounded label-icon-lg">-->
                    <!--    <i class="fa fa-facebook"></i>-->
                    <!--</a>-->
                    <!--<a href="#" class="label-icon label-icon-footer label-icon-bordered label-icon-rounded label-icon-lg">-->
                    <!--    <i class="fa fa-twitter"></i>-->
                    <!--</a>-->
                    <!--<a href="#" class="label-icon label-icon-footer label-icon-bordered label-icon-rounded label-icon-lg">-->
                    <!--    <i class="fa fa-youtube"></i>-->
                    <!--</a>-->
                    <!--<a href="#" class="label-icon label-icon-footer label-icon-bordered label-icon-rounded label-icon-lg">-->
                    <!--    <i class="fa fa-google-plus"></i>-->
                    <!--</a>-->
                    <!--<a href="#" class="label-icon label-icon-footer label-icon-bordered label-icon-rounded label-icon-lg">-->
                    <!--    <i class="fa fa-feed"></i>-->
                    <!--</a>

                </div>
            </div>
        </div>
        <div class="app-footer-line darken">
            <div class="copyright wide text-center">&copy;<a href=""> .</a></div>
        </div>
    </div>
   END APP FOOTER -->

    <!-- START APP SIDEPANEL -->

    <!-- END APP SIDEPANEL -->

    <!-- APP OVERLAY -->
    <div class="app-overlay"></div>
    <!-- END APP OVERLAY -->
</div>
<!-- END APP WRAPPER -->

<!-- IMPORTANT SCRIPTS -->
<script type="text/javascript" src="{{asset('admin/js/vendor/jquery/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/vendor/jquery/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/vendor/bootstrap/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/vendor/moment/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/vendor/customscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
<!-- END IMPORTANT SCRIPTS -->


@yield('footer')

<!-- END THIS PAGE SCRIPTS -->
<!-- APP SCRIPTS -->
<script type="text/javascript" src="{{asset('admin/js/app.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/app_plugins.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/app_demo.js')}}"></script>
<!-- END APP SCRIPTS -->
<script type="text/javascript" src="{{asset('admin/js/app_demo_dashboard.js')}}"></script>


<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
@if(Session::has('message'))

@if(Session::get('status', 'info')=="success")
<script type="text/javascript">
    $(function() {
    "use strict";

    var SweetAlert = function() {};

    //examples
    SweetAlert.prototype.init = function() {

    //Success Message
    setTimeout(function(){
        swal({
            title: "عملية ناجحة",
             type: "success",
            text: "تمت العملية بنجاح",
            confirmButtonColor: "#8BC34A",
            timer: 2000,
            showConfirmButton: false
        });
     }, 3000);


    },
    //init
    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert;

    $.SweetAlert.init();
});
</script>
@endif
@endif

  <script>
    $(document).ready(function() {
    if($('.datatable-extended').length) {
          $('.datatable-extended').DataTable({
              dom: 'Bfrtip',

              bDestroy: true,
              buttons: [
                  'copyHtml5',
                  'excelHtml5',
                  'csvHtml5',
                  'pdfHtml5'
              ]
          });
        }
    });
  </script>
  <script >
  $( document ).ready(function() {
  $( ".copyit" ).click(function( event ) {
    event.preventDefault();
    var id = $(this).attr('idk');
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(id).select();
    document.execCommand("copy");
    $temp.remove();
     alert("تم النسخ" );
  });
  });



  </script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

</body>
</html>
