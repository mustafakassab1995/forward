<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from coderthemes.com/uplon/layouts/horizontal/index-rtl.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 12 Feb 2020 17:32:22 GMT -->
<head>
        <meta charset="utf-8" />
        <title>Forward</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Responsive bootstrap 4 admin template" name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{asset('nass/images/favicon.ico')}}">

        <!-- App css -->
        <link href="{{asset('nass/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('nass/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('nass/css/app-rtl.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('nass/libs/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
      <link href="{{asset('nass/libs/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
      <link href="{{asset('nass/libs/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
      <link href="{{asset('nass/libs/datatables/select.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />

    </head>

    <body>

        <!-- Navigation Bar-->
        <header id="topnav">



            <!-- Topbar Start -->
            <div class="navbar-custom">
                <div class="container-fluid">
                    <ul class="list-unstyled topnav-menu float-right mb-0">

                        <li class="dropdown notification-list">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle nav-link">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </li>

                        {{-- <li class="dropdown notification-list dropdown d-none d-lg-inline-block ml-2">
                            <a class="nav-link dropdown-toggle mr-0 waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <img src="{{asset('nass/images/flags/us.jpg')}}" alt="lang-image" height="12">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <img src="{{asset('nass/images/flags/germany.jpg')}}" alt="lang-image" class="mr-1" height="12"> <span
                                        class="align-middle">German</span>
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <img src="{{asset('nass/images/flags/italy.jpg')}}" alt="lang-image" class="mr-1" height="12"> <span
                                        class="align-middle">Italian</span>
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <img src="{{asset('nass/images/flags/spain.jpg')}}" alt="lang-image" class="mr-1" height="12"> <span
                                        class="align-middle">Spanish</span>
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <img src="{{asset('nass/images/flags/russia.jpg')}}" alt="lang-image" class="mr-1" height="12"> <span
                                        class="align-middle">Russian</span>
                                </a>

                            </div>
                        </li> --}}

                        <li class="dropdown notification-list">
                            <a class="nav-link dropdown-toggle  waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <i class="mdi mdi-bell-outline noti-icon"></i>
                                <span class="noti-icon-badge"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-lg">

                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5 class="font-16 text-white m-0">
                                        <span class="float-right">
                                            <a href="#" class="text-white">
                                                <small>اخفاء الكل</small>
                                            </a>
                                        </span>الشعارات
                                    </h5>
                                </div>

                                <div class="slimscroll noti-scroll">

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-success">
                                            <i class="mdi mdi-settings-outline"></i>
                                        </div>
                                        <p class="notify-details">طلب جديد
                                            <small class="text-muted">هناك طلب جديد</small>
                                        </p>
                                    </a>



                                <!-- All-->
                                <a href="javascript:void(0);" class="dropdown-item text-primary notify-item notify-all">
                                    View all
                                    <i class="fi-arrow-right"></i>
                                </a>

                            </div>
                        </li>


                        <li class="dropdown notification-list">
                            <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <img src="{{asset('nass/images/users/avatar-1.jpg')}}" alt="user-image" class="rounded-circle">
                                <span class="d-none d-sm-inline-block ml-1 font-weight-medium">Alex M.</span>
                                <i class="mdi mdi-chevron-down d-none d-sm-inline-block"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                <!-- item-->
                                <div class="dropdown-header noti-title">
                                    <h6 class="text-overflow text-white m-0">مرحبا !</h6>
                                </div>





                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <i class="mdi mdi-logout-variant"></i>
                                    <span>تسجيل الخروج</span>
                                </a>

                            </div>
                        </li>

                        <li class="dropdown notification-list">
                            <a href="javascript:void(0);" class="nav-link right-bar-toggle waves-effect waves-light">
                                <i class="mdi mdi-settings-outline noti-icon"></i>
                            </a>
                        </li>

                    </ul>

                    <!-- LOGO -->
                    <div class="logo-box">
                        <a href="index.html" class="logo text-center logo-dark">
                            <span class="logo-lg">
                                <img src="{{asset('nass/images/logo.png')}}" alt="" height="22">
                                <!-- <span class="logo-lg-text-dark">Uplon</span> -->
                            </span>
                            <span class="logo-sm">
                                <!-- <span class="logo-lg-text-dark">U</span> -->
                                <img src="{{asset('nass/images/logo-sm.png')}}" alt="" height="24">
                            </span>
                        </a>

                        <a href="index.html" class="logo text-center logo-light">
                            <span class="logo-lg">
                                <img src="{{asset('nass/images/logo-light.png')}}" alt="" height="22">
                                <!-- <span class="logo-lg-text-dark">Uplon</span> -->
                            </span>
                            <span class="logo-sm">
                                <!-- <span class="logo-lg-text-dark">U</span> -->
                                <img src="{{asset('nass/images/logo-sm-light.png')}}" alt="" height="24">
                            </span>
                        </a>
                    </div>

                    <ul class="list-unstyled topnav-menu topnav-menu-left m-0">

                        <li class="d-none d-md-block">
                            <form class="app-search">
                                <div class="app-search-box">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search...">
                                        <div class="input-group-append">
                                            <button class="btn" type="submit">
                                                <i class="fas fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </li>

                      </ul>

                </div> <!-- end container-fluid-->
            </div>
            <!-- end Topbar -->

            @include('admin.navi');

        </header>
        <!-- End Navigation Bar-->

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="wrapper">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">اللوحة</a></li>
                                    <li class="breadcrumb-item active">{{$title_ar ?? ''}}</li>
                                </ol>
                            </div>
                            <h4 class="page-title">{{$title_ar ?? ''}}</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title -->
                @yield('content')

                <!-- end row -->
            </div> <!-- end container-fluid -->
        </div>
        <!-- end wrapper -->

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->



        <!-- Footer Start -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        2016 - 2020 &copy;  <a href="#">Forward App </a>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->

        <!-- Right Sidebar -->
        <div class="right-bar">
            <div class="rightbar-title">
                <a href="javascript:void(0);" class="right-bar-toggle float-right">
                    <i class="mdi mdi-close"></i>
                </a>
                <h4 class="font-18 m-0 text-white"> Forword App</h4>
            </div>
            <div class="slimscroll-menu">

                <div class="p-4">
                    <div class="alert alert-warning" role="alert">
                        <strong> Forword App</strong> .
                    </div>
                    <div class="mb-2">
                        <img src="{{asset('nass/images/layouts/light.png')}}" class="img-fluid img-thumbnail" alt="">
                    </div>
                    <div class="custom-control custom-switch mb-3">
                        <input type="checkbox" class="custom-control-input theme-choice" id="light-mode-switch" checked />
                        <label class="custom-control-label" for="light-mode-switch">Light Mode</label>
                    </div>

                    <div class="mb-2">
                        <img src="{{asset('nass/images/layouts/dark.png')}}" class="img-fluid img-thumbnail" alt="">
                    </div>
                    <div class="custom-control custom-switch mb-3">
                        <input type="checkbox" class="custom-control-input theme-choice" id="dark-mode-switch" data-bsStyle="assets/css/bootstrap-dark.min.css"
                            data-appStyle="assets/css/app-dark.min.css" />
                        <label class="custom-control-label" for="dark-mode-switch">Dark Mode</label>
                    </div>

                    <div class="mb-2">
                        <img src="{{asset('nass/images/layouts/rtl.png')}}" class="img-fluid img-thumbnail" alt="">
                    </div>
                    <div class="custom-control custom-switch mb-5">
                        <input type="checkbox" class="custom-control-input theme-choice" id="rtl-mode-switch" data-appStyle="assets/css/app-rtl.min.css" />
                        <label class="custom-control-label" for="rtl-mode-switch">RTL Mode</label>
                    </div>

                    <a href="https://1.envato.market/XY7j5" class="btn btn-danger btn-block mt-3" target="_blank"><i class="mdi mdi-download mr-1"></i> Download Now</a>
                </div>
            </div> <!-- end slimscroll-menu-->
        </div>
        <!-- /Right-bar -->

        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- Vendor js -->
        <script src="{{asset('nass/js/vendor.min.js')}}"></script>

        <!--Morris Chart-->
        <script src="{{asset('nass/libs/morris-js/morris.min.js')}}"></script>
        <script src="{{asset('nass/libs/raphael/raphael.min.js')}}"></script>

        <!-- Dashboard init js-->
        <script src="{{asset('nass/js/pages/dashboard.init.js')}}"></script>

        <!-- App js -->
        <script src="{{asset('nass/js/app.min.js')}}"></script>

        <!-- Datatable plugin js -->
      <script src="{{asset('nass/libs/datatables/jquery.dataTables.min.js')}}"></script>
      <script src="{{asset('nass/libs/datatables/dataTables.bootstrap4.min.js')}}"></script>

      <script src="{{asset('nass/libs/datatables/dataTables.responsive.min.js')}}"></script>
      <script src="{{asset('nass/libs/datatables/responsive.bootstrap4.min.js')}}"></script>

      <script src="{{asset('nass/libs/datatables/dataTables.buttons.min.js')}}"></script>
      <script src="{{asset('nass/libs/datatables/buttons.bootstrap4.min.js')}}"></script>

      <script src="{{asset('nass/libs/jszip/jszip.min.js')}}"></script>
      <script src="{{asset('nass/libs/pdfmake/pdfmake.min.js')}}"></script>
      <script src="{{asset('nass/libs/pdfmake/vfs_fonts.js')}}"></script>

      <script src="{{asset('nass/libs/datatables/buttons.html5.min.js')}}"></script>
      <script src="{{asset('nass/libs/datatables/buttons.print.min.js')}}"></script>

      <script src="{{asset('nass/libs/datatables/dataTables.keyTable.min.js')}}"></script>
      <script src="{{asset('nass/libs/datatables/dataTables.select.min.js')}}"></script>

      <!-- Datatables init -->
      <script src="{{asset('nass/js/pages/datatables.init.js')}}"></script>

    </body>

<!-- Mirrored from coderthemes.com/uplon/layouts/horizontal/index-rtl.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 12 Feb 2020 17:33:02 GMT -->
</html>
