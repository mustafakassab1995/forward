<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('details');
            $table->integer('client_id');
            $table->integer('business_id');
            $table->integer('driver_id');
            $table->integer('collecter_id');
            $table->string('items_total');
            $table->string('who_pays_delivery');
            $table->string('net_price');
            $table->string('delivery_cost');
            $table->string('total');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
