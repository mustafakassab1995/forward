<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::middleware('auth')->group(function () {
Route::middleware('admin')->prefix('admin')->group(function () {
  Route::get('index', function(){
    return view('admin.index');
  });

  Route::resource('Order', 'OrderController');
  Route::resource('User', 'UserController');
  Route::resource('Driver', 'DriverController');
  Route::resource('Client', 'ClientController');
  Route::resource('Business', 'BusinessController');
 Route::resource('Collector', 'CollectorController');




});
});
