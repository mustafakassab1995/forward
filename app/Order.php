<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //

    protected $fillable = [
       'details', 'client_id',
      'driver_id',
      'business_id', 'collecter_id',
      'items_total', 'who_pays_delivery',
      'net_price', 'delivery_cost',
     'total','status'
    ];

    public function  getCollectoriAttribute($value){
       return getTheUser($this->attributes['collecter_id']);
   }

   public function  getBusinessiAttribute($value){
      return getTheBusiness($this->attributes['business_id']);
  }

  public function  getDriveriAttribute($value){
     return getTheBusiness($this->attributes['driver_id']);
 }

 public function  getClientiAttribute($value){
    return getTheBusiness($this->attributes['client_id']);
}
}
