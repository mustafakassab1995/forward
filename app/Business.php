<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    //
    protected $fillable = [
      'name_ar', 'name_en', 'business_field',
      'business_type', 'address', 'sub_address',
      'sub_sub_address', 'lat', 'lng',
        'business_phone', 'lang', 'user_id',
    ];
    public function user(){
      return $this->belongsTo('App\User');
    }

    public function  getUseriAttribute($value){
       return getTheUser($this->attributes['user_id']);
   }
}
