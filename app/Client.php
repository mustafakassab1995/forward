<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //
    protected $fillable = [
       'address', 'sub_address',
      'sub_sub_address',
     'business_id', 'user_id',
    ];

    public function  getUseriAttribute($value){
       return getTheUser($this->attributes['user_id']);
   }

   public function  getBusinessiAttribute($value){
      return getTheBusiness($this->attributes['business_id']);
  }
}
