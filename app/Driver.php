<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    //
    protected $fillable = [
      'car_color', 'plate_number',
       'address', 'availability_time',
      'car_type',
     'car_year', 'user_id',
    ];

    public function  getUseriAttribute($value){
       return getTheUser($this->attributes['user_id']);
   }

   
}
