<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ClientController extends Controller
{
          public $data = [];
        public $Model = "";
        public $modelName= "";
        public function edit($id)
        {
          //
          $this->_construct();
          $this->data['item']=$this->Model::findOrFail($id)->toArray();
          $this->data['form_action']=$this->modelName .".update";
            $this->data['subtitle'] ="Edit A ".$this->modelName ;
          return view('admin.forms.edit',$this->data);
        }
        public function _construct(){
        $this->modelName = "Client";
        $this->data['title']=$this->modelName;
        $this->data['title_ar']="الزبائن";
        $this->Model = '\App\\'.$this->modelName;
        $this->mRoute = $this->modelName;



                  $data_productss = \App\User::all();
         $data_products =[];

         for ($i=0; $i <count($data_productss) ; $i++) {
          $data_products[]=["key"=>$data_productss[$i]['id'],'label'=>$data_productss[$i]['name']];
         }
         $data_productss =[];

         $data_productss = \App\Business::all();
       $data_productsB =[];

       for ($i=0; $i <count($data_productss) ; $i++) {
        $data_productsB[]=["key"=>$data_productss[$i]['id'],'label'=>$data_productss[$i]['name_ar'].'-'.$data_productss[$i]['useri']['name'].'-'.$data_productss[$i]['useri']['phone']];
       }

        $this->data['form_method']="post";
        $this->data['form_multipart']=true;
        $this->data['form_atts']=[];
        $this->data['form_atts'][] = ['name'=>'address','input'=>'input','type'=>'text','label'=>'العنوان','required'=>true];
        $this->data['form_atts'][] = ['name'=>'sub_address','input'=>'input','type'=>'text','label'=>'العنوان الفرعي','required'=>true];
        $this->data['form_atts'][] = ['name'=>'sub_sub_address','input'=>'input','type'=>'text','label'=>'عنوان فرع الفرعي','required'=>true];
        $this->data['form_atts'][] = ['name'=>'user_id','input'=>'select','type'=>'select','label'=>'المستخدم التابع له','required'=>true,
        'data'=>$data_products];

        $this->data['form_atts'][] = ['name'=>'business_id','input'=>'select','type'=>'select','label'=>'صاحب العمل ','required'=>true,
        'data'=>$data_productsB];
        for ($i=0; $i < count($this->data['form_atts']) ; $i++) {
         $this->data['form_atts'][$i]['label'] = $this->makeLabel($this->data['form_atts'][$i]['label']);
        }
        }
        function makeLabel($string){
         $label = str_replace("_"," ",$string);
        $label =  ucwords($label);
        return $label;
        }
        public function index(Request $request)
        {
         //

        $this->_construct();
        $this->data['subtitle'] ="Show All ".$this->modelName ;
        $this->data['instances'] = $this->Model::orderBy('id','desc')->get()->toArray();

        if($request->get('type'))
        $this->data['instances'] = $this->Model::where('type','=',$request->get('type'))->orderBy('id','desc')->get()->toArray();
        return view('admin.forms.index',$this->data);
        }
        /**
        * Show the form for creating a new resource.
        *
        * @return \Illuminate\Http\Response
        */
          public function create()
          {
             //
             $this->_construct();

             $this->data['subtitle'] ="Create A New ".$this->modelName ;

             $this->data['form_action']=$this->modelName.".store";

             return view('admin.forms.create',$this->data);
          }
          public function store(Request $request)
         {

           $this->_construct();



           $in = $request->except(['_token']);

               $this->Model::create($in);

               \Session::flash('message', 'Item was added successfully');
              \Session::flash('status', 'success');
                 return redirect()->route($this->mRoute.'.index');
           }



        public function update(Request $request, $id)
        {
          $this->_construct();

          $in = $request->except(['_token','_method']);
              $this->Model::where('id',$id)->update($in);
           \Session::flash('message', 'Item was updated successfully');
          \Session::flash('status', 'success');
            return redirect()->route($this->mRoute.'.index');
        }

        public function destroy($id)
        {
           //
           $this->_construct();

           \Session::flash('message', 'Item was Deleted successfully');
          \Session::flash('status', 'success');
           $this->Model::destroy($id);
           return redirect()->route($this->mRoute.'.index');
        }

}
