<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    //
    public $data = [];
  public $Model = "";
  public $modelName= "";
  public function edit($id)
  {
    //
    $this->_construct();
    $this->data['item']=$this->Model::findOrFail($id)->toArray();
    $this->data['form_action']=$this->modelName .".update";
      $this->data['subtitle'] ="Edit A ".$this->modelName ;
    return view('admin.forms.edit',$this->data);
  }
  public function _construct(){
  $this->modelName = "User";
  $this->data['title']=$this->modelName;
  $this->data['title_ar']="المستخدمين";
  $this->Model = '\App\\'.$this->modelName;
    $this->mRoute = $this->modelName;



  $this->data['form_method']="post";
  $this->data['form_multipart']=true;
  $this->data['form_atts']=[];
  $this->data['form_atts'][] = ['name'=>'name','input'=>'input','type'=>'text','label'=>'الإسم','required'=>true];
  $this->data['form_atts'][] = ['name'=>'phone','input'=>'input','type'=>'text','label'=>'رقم الجوال','required'=>true];
  $this->data['form_atts'][] = ['name'=>'password','input'=>'input','type'=>'password','label'=>' كلمة السر','required'=>true];
  $this->data['form_atts'][] = ['name'=>'email','input'=>'input','type'=>'email','label'=>'الايميل','required'=>true];
  $this->data['form_atts'][] = ['name'=>'type','input'=>'select','type'=>'select','label'=>'النوع','required'=>true,'data'=>[
    ['key'=>"admin",'label'=>'ادمن'],
    ['key'=>"client",'label'=>'زبون'],
    ['key'=>"collector",'label'=>'محصل'],
    ['key'=>"driver",'label'=>'سائق'],
    ['key'=>"business",'label'=>'صاحب عمل'],
    ]];


  for ($i=0; $i < count($this->data['form_atts']) ; $i++) {
   $this->data['form_atts'][$i]['label'] = $this->makeLabel($this->data['form_atts'][$i]['label']);
  }
  }
  function makeLabel($string){
   $label = str_replace("_"," ",$string);
  $label =  ucwords($label);
  return $label;
  }
  public function index(Request $request)
  {
   //

  $this->_construct();
  $this->data['subtitle'] ="Show All ".$this->modelName ;
  $this->data['instances'] = $this->Model::orderBy('id','desc')->get()->toArray();

  if($request->get('type'))
  $this->data['instances'] = $this->Model::where('type','=',$request->get('type'))->orderBy('id','desc')->get()->toArray();
  return view('admin.forms.index',$this->data);
  }
  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
    public function create()
    {
       //
       $this->_construct();

       $this->data['subtitle'] ="Create A New ".$this->modelName ;

       $this->data['form_action']=$this->modelName.".store";

       return view('admin.forms.create',$this->data);
    }
    public function store(Request $request)
   {


     $this->_construct();


     $in = $request->except(['_token']);
     $in['password']=bcrypt($in['password']);
         $this->Model::create($in);

         \Session::flash('message', 'Item was added successfully');
        \Session::flash('status', 'success');
           return redirect()->route($this->mRoute.'.index');
     }



  public function update(Request $request, $id)
  {
    $this->_construct();

    $in = $request->except(['_token','_method']);
    $in['password']=bcrypt($in['password']);
    
        $this->Model::where('id',$id)->update($in);
     \Session::flash('message', 'Item was updated successfully');
    \Session::flash('status', 'success');
      return redirect()->route($this->mRoute.'.index');
  }

  public function destroy($id)
  {
     //
     $this->_construct();

     \Session::flash('message', 'Item was Deleted successfully');
    \Session::flash('status', 'success');
     $this->Model::destroy($id);
     return redirect()->route($this->mRoute.'.index');
  }

}
